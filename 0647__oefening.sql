CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumReleasesLoop`(in extraReleases int)
BEGIN
declare counter int default 0;
Loup: Loop

if counter >= extraReleases then

leave Loup;
else 
call MockAlbumReleaseSucces(@succes);
if @succes=1 then
select counter;
set counter=counter + 1;
end if;
end if;
end loop;

END