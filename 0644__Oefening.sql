CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumRelease`()
BEGIN
declare numberOfAlbums INT DEFAULT 0; 
declare numberOfBands INT DEFAULT 0;
declare randomAlbumId int default 0;
declare randomBandId int default 0;
declare existId1 int default 0;
declare existId2 int default 0;

select count(*)
into numberOfAlbums
from albums;
select count(*)
into numberOfBands
from bands;
set randomAlbumId =floor(rand() * numberOfAlbums)+1;
set randomBandId=floor(rand()*numberOfBands)+1;

if ((select count(*) from Albumreleases where Bands_Id=randomBandId)<1 and (select count(*) from Albumreleases where Albums_Id=randomAlbumId)<1 ) then

insert into albumreleases (Bands_Id,Albums_Id) values (randomBandId,randomAlbumId);



end if;



END