CREATE DEFINER=`root`@`localhost` PROCEDURE `CleanupOldMemberships`(in datum date, out deleted int)
BEGIN
set sql_safe_updates=0;
select count(*)
into deleted
from lidmaatschappen where einddatum is not null and einddatum < datum;
DELETE from lidmaatschappen
where einddatum IS NOT NULL and einddatum < datum;
set sql_safe_updates=1;
END