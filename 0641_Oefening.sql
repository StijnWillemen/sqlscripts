CREATE DEFINER=`root`@`localhost` PROCEDURE `NumberOfGenres`(OUT aantal TINYINT)
BEGIN
select distinct COUNT(*)
into aantal
from genres;

END